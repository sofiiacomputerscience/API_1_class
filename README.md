
# Big Data API Course Project

## Overview

This repository contains the practical works submitted by Sofiia Boldeskul for the Big Data API course, under the guidance of Professor Gerald Roncajolo at ISEN School. The course focuses on the application and management of APIs in the context of big data.

## Contents

The repository is organized into the following practical works:

### Practical Work 1: Patient API and Postman Collection
- `Patient_API.json`: This file includes the API structure and schema for handling patient data.
- `API_first_class.postman_collection.json`: A Postman collection that demonstrates the usage of the Patient API, including sample requests and responses.

### Practical Works 2 & 3: Practice Manager
- `practicemanager-main 2`: This folder contains the second and third practical works. It includes an implementation of API end-points and their security + role-based access control.

## Instructions for Use
- To use the Patient API, import `Patient_API.json` into your API management tool.
- For testing and exploring API endpoints, import `API_first_class.postman_collection.json` into Postman.
- For Practical Works 2 & 3, refer to the README in the `practicemanager-main 2` folder for detailed instructions and documentation.

## Contributing
This project is part of an academic course and is not open for external contributions. However, feedback and suggestions from fellow students and faculty members are welcome.


## Acknowledgements
Special thanks to Professor Gerald Roncajolo for his guidance and support throughout this course.

