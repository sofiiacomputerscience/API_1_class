from datetime import datetime

from patient.domain.patient import Patient
from patient.infrastructure.patient_entity import PatientEntity


class PatientService:
    def __init__(self, patient_repository):
        self.patient_repository = patient_repository

    def create_patient(self, patient: Patient) -> PatientEntity:
        required_fields = ['first_name', 'last_name', 'date_of_birth', 'social_security_number']

        if any(not getattr(patient, field, None) for field in required_fields):
            raise ValueError('Patient first name is required')

        if patient.date_of_birth != datetime.strptime(patient.date_of_birth, '%Y-%m-%d') \
                .strftime('%Y-%m-%d'):
            raise ValueError('Patient date of birth is invalid')

        created_patient = self.patient_repository.add_patient(patient)
        return created_patient
    
    def update_patient(self, patient_id: int, updated_data: Patient) -> PatientEntity:
        updated_patient = self.patient_repository.update_patient(patient_id, updated_data)
        if not updated_patient:
            raise ValueError('Patient not found')
        return updated_patient

    def delete_patient(self, patient_id: int):
        self.patient_repository.delete_patient(patient_id)

    def get_patient_by_id(self, patient_id: int) -> PatientEntity:
        patient = self.patient_repository.get_patient(patient_id)
        if not patient:
            raise ValueError('Patient not found')
        return patient

    def search_patients(self, criteria: dict) -> list:
        return self.patient_repository.search_patients(criteria)

    def list_all_patients(self) -> list:
        return self.patient_repository.list_all_patients()

    def list_patients_paginated(self, page: int, per_page: int) -> (list, int):
        return self.patient_repository.list_patients_paginated(page, per_page)

