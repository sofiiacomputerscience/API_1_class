from flask import request, jsonify, Blueprint, abort
from flask import current_app as app
from flask_oidc import OpenIDConnect
from jose import jwt
from json import JSONEncoder

from patient.domain.patient import Patient

patient_bp = Blueprint('patient', __name__)
oidc = OpenIDConnect()

@patient_bp.route('/patients', methods=['POST'])
def create_patient():
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        created_patient = patient_service.create_patient(Patient(**patient_data))

        return jsonify(created_patient.to_dict()), 201
    except Exception as e:
        return jsonify({'error': str(e)}), 400


@oidc.accept_token()
@patient_bp.route('/patients/<int:patient_id>', methods=['PUT'])
def update_patient(patient_id):
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        updated_patient = patient_service.update_patient(patient_id, Patient(**patient_data))

        return jsonify(updated_patient.to_dict()), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@oidc.accept_token()
@patient_bp.route('/patients/<int:patient_id>', methods=['DELETE'])
def delete_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient_service.delete_patient(patient_id)

        return jsonify({'message': 'Patient deleted successfully'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400
    

@patient_bp.route('/patients/<int:patient_id>', methods=['GET'])
def get_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient = patient_service.get_patient_by_id(patient_id)

        return jsonify(patient.to_dict()), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@patient_bp.route('/patients', methods=['GET'])
def list_or_search_patients():
    try:
        search_criteria = request.args
        patient_service = app.config['patient_service']

        if search_criteria:
            patients = patient_service.search_patients(search_criteria)
        else:
            patients = patient_service.list_all_patients()

        return jsonify([patient.to_dict() for patient in patients]), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@patient_bp.route('/patients/paginated', methods=['GET'])
def list_patients_paginated():
    try:
        page = request.args.get('page', 1, type=int)
        per_page = request.args.get('per_page', 10, type=int)
        patient_service = app.config['patient_service']
        patients, total = patient_service.list_patients_paginated(page, per_page)

        return jsonify({'patients': [patient.to_dict() for patient in patients], 'total': total}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400
    
@patient_bp.route('/doctor/patients', methods=['GET'])
@oidc.accept_token()
def get_patients_for_doctor():
    token = request.headers.get('Authorization').split(' ')[1]
    claims = jwt.get_unverified_claims(token)
    
    if 'doctor' not in claims.get('roles', []):
        abort(403, 'Access forbidden. Only users with the "doctor" role can access this endpoint.')

    patient_service = app.config['patient_service']
    patients = patient_service.list_all_patients()
    return jsonify([patient.to_dict() for patient in patients]), 200

