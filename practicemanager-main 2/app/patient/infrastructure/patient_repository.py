from patient.domain.patient import Patient
from patient.infrastructure.patient_entity import PatientEntity


class PatientRepository:
    def __init__(self):
        self.db = {}
        self.next_patient_id = 1

    def add_patient(self, patient: Patient) -> PatientEntity:
       
        patient_id = self.next_patient_id
        self.next_patient_id += 1

        
        patient_entity = PatientEntity(
            id=patient_id,
            first_name=patient.first_name,
            last_name=patient.last_name,
            date_of_birth=patient.date_of_birth,
            social_security_number=patient.social_security_number
        )

        
        self.db[patient_id] = patient_entity

        
        return patient_entity

    def get_patient(self, patient_id):
        return self.db.get(patient_id)
    
    def update_patient(self, patient_id: int, updated_data: Patient) -> PatientEntity:
        if patient_id not in self.db:
            return None  
        
        
        patient_entity = self.db[patient_id]
        patient_entity.first_name = updated_data.first_name
        patient_entity.last_name = updated_data.last_name
        patient_entity.date_of_birth = updated_data.date_of_birth
        patient_entity.social_security_number = updated_data.social_security_number

        return patient_entity

    def delete_patient(self, patient_id: int):
        if patient_id in self.db:
            del self.db[patient_id]

    def search_patients(self, criteria: dict) -> list:
        results = []
        for patient in self.db.values():
            if all(str(getattr(patient, key, None)) == value for key, value in criteria.items()):
                results.append(patient)
        return results


    def list_all_patients(self) -> list:
        return list(self.db.values())

    def list_patients_paginated(self, page: int, per_page: int) -> (list, int):
        start = (page - 1) * per_page
        end = start + per_page
        return list(self.db.values())[start:end], len(self.db)