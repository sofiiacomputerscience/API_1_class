import unittest
from patient.domain.patient import Patient
from patient.infrastructure.patient_entity import PatientEntity
from patient.infrastructure.patient_repository import PatientRepository


class TestPatientRepository(unittest.TestCase):
    def setUp(self):
        self.repository = PatientRepository()

    def test_add_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        self.assertEqual(self.repository.get_patient(new_patient.id), PatientEntity(**{'id': 1, **patient_data}))

    def test_get_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        retrieved_patient = self.repository.get_patient(new_patient.id)

        self.assertEqual(retrieved_patient, PatientEntity(**{'id': 1, **patient_data}))

    def test_update_patient(self):
        # Add a patient
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '1234567890'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        # Update the patient
        updated_data = {
            'first_name': 'Sofiia',
            'last_name': 'Boldeskul',
            'date_of_birth': '2002-12-20',
            'social_security_number': '0987654321'
        }
        self.repository.update_patient(new_patient.id, Patient(**updated_data))
        
        # Retrieve and verify the updated patient data
        updated_patient = self.repository.get_patient(new_patient.id)
        self.assertEqual(updated_patient.first_name, updated_data['first_name'])
        self.assertEqual(updated_patient.last_name, updated_data['last_name'])
        self.assertEqual(updated_patient.date_of_birth, updated_data['date_of_birth'])
        self.assertEqual(updated_patient.social_security_number, updated_data['social_security_number'])

    def test_delete_patient(self):
        # Add a patient
        patient_data = {
            'first_name': 'Alice',
            'last_name': 'Amiot',
            'date_of_birth': '1990-02-02',
            'social_security_number': '2223334444'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        # Delete the patient
        self.repository.delete_patient(new_patient.id)
        
        # Verify the patient is no longer in the repository
        self.assertIsNone(self.repository.get_patient(new_patient.id))

    def test_search_patients(self):
        # Add two patients
        patient_data_1 = {
            'first_name': 'Emily',
            'last_name': 'Clark',
            'date_of_birth': '1990-01-01',
            'social_security_number': '5556667777'
        }
        patient_data_2 = {
            'first_name': 'Mieke',
            'last_name': 'Clark',
            'date_of_birth': '1987-05-05',
            'social_security_number': '8889990000'
        }
        self.repository.add_patient(Patient(**patient_data_1))
        self.repository.add_patient(Patient(**patient_data_2))

        # Search for patients with last name "Clark"
        search_criteria = {'last_name': 'Clark'}
        found_patients = self.repository.search_patients(search_criteria)

        # Verify the correct patients are found
        self.assertEqual(len(found_patients), 2)
        for patient in found_patients:
            self.assertEqual(patient.last_name, 'Clark')

    def test_list_all_patients(self):
        # Assuming the repository already has some patients added
        all_patients = self.repository.list_all_patients()

        # Verify all patients are listed
        self.assertTrue(len(all_patients) >= 2)  # Adjust this based on the expected number of patients

    def test_list_patients_paginated(self):
        # Assuming the repository already has more patients than the per_page limit
        page = 1
        per_page = 2
        paginated_patients, total = self.repository.list_patients_paginated(page, per_page)

        # Verify the correct patients are listed in the page and total count is correct
        self.assertEqual(len(paginated_patients), per_page)
        self.assertTrue(total >= len(paginated_patients))

if __name__ == '__main__':
    unittest.main()