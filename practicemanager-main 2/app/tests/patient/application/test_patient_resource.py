import unittest
from unittest.mock import Mock
from flask.testing import FlaskClient
from patient.domain.patient import Patient


from app import create_app
from patient.application.patient_service import PatientService
from patient.infrastructure.patient_entity import PatientEntity


class TestPatientResource(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.app.config['TESTING'] = True
        self.client: FlaskClient = self.app.test_client()

        self.mock_service = Mock(spec=PatientService)
        self.app.config['patient_service'] = self.mock_service

    def test_create_patient_endpoint(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345
        }

        self.mock_service.create_patient.return_value = PatientEntity(**{'id': 1, **patient_data})

        response = self.client.post('/patients', json=patient_data)

        self.assertEqual(201, response.status_code)
        print(response.json)
        self.assertEqual(response.json, {'id': 1, **patient_data})
        self.mock_service.create_patient.assert_called_once()
    
    def test_update_patient_endpoint(self):
        updated_patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-02-01',
            'social_security_number': 123456789012345
        }
        patient_id = 1

        self.mock_service.update_patient.return_value = PatientEntity(**{'id': patient_id, **updated_patient_data})

        response = self.client.put(f'/patients/{patient_id}', json=updated_patient_data)

        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json, {'id': patient_id, **updated_patient_data})
        self.mock_service.update_patient.assert_called_once_with(patient_id, Patient(**updated_patient_data))

    def test_delete_patient_endpoint(self):
        patient_id = 1

        response = self.client.delete(f'/patients/{patient_id}')

        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json, {'message': 'Patient deleted successfully'})
        self.mock_service.delete_patient.assert_called_once_with(patient_id)

    def test_get_patient_endpoint(self):
        patient_id = 1
        patient_data = {
            'id': patient_id,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345
        }

        self.mock_service.get_patient_by_id.return_value = PatientEntity(**patient_data)

        response = self.client.get(f'/patients/{patient_id}')

        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json, patient_data)
        self.mock_service.get_patient_by_id.assert_called_once_with(patient_id)

    def test_list_or_search_patients_endpoint(self):
        patient_data_1 = {
            'id': 1,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345
        }
        patient_data_2 = {
            'id': 2,
            'first_name': 'Jane',
            'last_name': 'Doe',
            'date_of_birth': '1985-05-05',
            'social_security_number': 123456789012346
        }
        self.mock_service.list_all_patients.return_value = [PatientEntity(**patient_data_1), PatientEntity(**patient_data_2)]

        # Test listing all patients
        response = self.client.get('/patients')

        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json, [patient_data_1, patient_data_2])
        self.mock_service.list_all_patients.assert_called_once()

        # Test searching patients
        search_criteria = {'first_name': 'John'}
        self.mock_service.search_patients.return_value = [PatientEntity(**patient_data_1)]
        response = self.client.get('/patients', query_string=search_criteria)

        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json, [patient_data_1])
        self.mock_service.search_patients.assert_called_once_with(search_criteria)

    def test_list_patients_paginated_endpoint(self):
        # Assuming the logic for pagination is handled in the service layer
        patient_data = {
            'id': 1,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345
        }
        self.mock_service.list_patients_paginated.return_value = ([PatientEntity(**patient_data)], 1)

        response = self.client.get('/patients/paginated', query_string={'page': 1, 'per_page': 10})

        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json, {'patients': [patient_data], 'total': 1})
        self.mock_service.list_patients_paginated.assert_called_once_with(1, 10)

