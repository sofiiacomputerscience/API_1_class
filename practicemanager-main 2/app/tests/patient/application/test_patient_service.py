import unittest
import unittest.mock as mock
from patient.application.patient_service import PatientService
from patient.domain.patient import Patient


class TestPatientService(unittest.TestCase):
    def setUp(self):
        self.mock_repository = mock.Mock()
        self.patient_service = PatientService(self.mock_repository)

    def test_create_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345
        }

        self.mock_repository.add_patient.return_value = {'id': 1, **patient_data}

        new_patient = self.patient_service.create_patient(Patient(**patient_data))

        self.assertIsNotNone(new_patient)
        self.assertEqual(new_patient['id'], 1)
        self.assertEqual(new_patient['first_name'], 'John')
        self.assertEqual(new_patient['last_name'], 'Doe')
        self.assertEqual(new_patient['date_of_birth'], '1980-01-01')
        self.assertEqual(new_patient['social_security_number'], 123456789012345)

        self.mock_repository.add_patient.assert_called_once()

    def create_patient_with_missing_value_raises_value_error(self, missing_field):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        del patient_data[missing_field]

        with self.assertRaises(ValueError):
            self.patient_service.create_patient(patient_data)

    def test_create_patient_with_missing_first_name(self):
        self.create_patient_with_missing_value_raises_value_error('first_name')

    def test_create_patient_with_missing_last_name(self):
        self.create_patient_with_missing_value_raises_value_error('last_name')

    def test_create_patient_with_missing_date_of_birth(self):
        self.create_patient_with_missing_value_raises_value_error('date_of_birth')

    def test_create_patient_with_missing_social_security_number(self):
        self.create_patient_with_missing_value_raises_value_error('social_security_number')

    def test_create_patient_with_invalid_birth_date(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-13-01',
            'social_security_number': '123456789012345'
        }

        with self.assertRaises(ValueError):
            self.patient_service.create_patient(patient_data)

        self.mock_repository.add_patient.assert_not_called()

    def test_update_patient(self):
        patient_id = 1
        updated_data = {
            'first_name': 'Jane',
            'last_name': 'Doe',
            'date_of_birth': '1980-02-01',
            'social_security_number': '123456789012346'
        }

        self.mock_repository.update_patient.return_value = {'id': patient_id, **updated_data}

        updated_patient = self.patient_service.update_patient(patient_id, Patient(**updated_data))

        self.assertIsNotNone(updated_patient)
        self.assertEqual(updated_patient['id'], patient_id)

    def test_delete_patient(self):
        patient_id = 1

        self.patient_service.delete_patient(patient_id)

        self.mock_repository.delete_patient.assert_called_once_with(patient_id)

    def test_get_patient_by_id(self):
        patient_id = 1
        patient_data = {
            'id': patient_id,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        self.mock_repository.get_patient.return_value = patient_data

        patient = self.patient_service.get_patient_by_id(patient_id)

        self.assertIsNotNone(patient)
        self.assertEqual(patient['id'], patient_id)
        self.mock_repository.get_patient.assert_called_once_with(patient_id)

    def test_search_patients(self):
        search_criteria = {'first_name': 'John'}
        patient_data = {
            'id': 1,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        self.mock_repository.search_patients.return_value = [patient_data]

        patients = self.patient_service.search_patients(search_criteria)

        self.assertEqual(len(patients), 1)
        self.assertEqual(patients[0], patient_data)
        self.mock_repository.search_patients.assert_called_once_with(search_criteria)

    def test_list_all_patients(self):
        patient_data_1 = {
            'id': 1,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient_data_2 = {
            'id': 2,
            'first_name': 'Jane',
            'last_name': 'Doe',
            'date_of_birth': '1980-02-01',
            'social_security_number': '123456789012346'
        }

        self.mock_repository.list_all_patients.return_value = [patient_data_1, patient_data_2]

        patients = self.patient_service.list_all_patients()

        self.assertEqual(len(patients), 2)
        self.assertEqual(patients[0], patient_data_1)
        self.assertEqual(patients[1], patient_data_2)
        self.mock_repository.list_all_patients.assert_called_once()

    def test_list_patients_paginated(self):
        page = 1
        per_page = 2
        total_patients = 2
        patient_data_1 = {
            'id': 1,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient_data_2 = {
            'id': 2,
            'first_name': 'Jane',
            'last_name': 'Doe',
            'date_of_birth': '1980-02-01',
            'social_security_number': '123456789012346'
        }

        self.mock_repository.list_patients_paginated.return_value = ([patient_data_1, patient_data_2], total_patients)

        patients, total = self.patient_service.list_patients_paginated(page, per_page)

        self.assertEqual(len(patients), 2)
        self.assertEqual(patients[0], patient_data_1)
        self.assertEqual(patients[1], patient_data_2)
        self.assertEqual(total, total_patients)
        self.mock_repository.list_patients_paginated.assert_called_once_with(page, per_page)



if __name__ == '__main__':
    unittest.main()
