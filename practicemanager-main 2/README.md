# Practice Manager

## First US and acceptance tests

As a doctor Thomas  
I want to keep records of my patients  
So that i can access them later

Acceptance criteria / Acceptance tests

Scenario: Create a patient  
// done: the creation of patient is succesfully done with curl command and POST request.
Given I am a doctor Thomas  
When I create a patient John Doe with the social security number 123456789123456    
Then I should see the patient in the list of patients

## A curl example to create a patient:

```bash
curl -v -X POST -H "Content-Type: application/json" \
  -d '{"first_name": "John", "last_name": "Doe", "date_of_birth": "1980-01-01", "social_security_number": 123456789123456}' \
  http://127.0.0.1:5000/patients
```


Scenario: Search a patient by social security number
// done: the searching for patient is succesfully done with curl command and GET request.
Given I am a doctor Thomas   
And I have a patient John Doe with the social security number 123456789123456    
When I search for the patient with the social security number 123456789123456  
Then I should see the patient John Die in the list of patients  

## A curl example to search a patient by Social Security Number:

```bash
curl "http://127.0.0.1:5000/patients?social_security_number=123456789123456"

```

Scenario: Search a patient by name  
Given I am a doctor Thomas  
And I have a patient John Doe with the social security number 123456789123456  
When I search for the patient with the name John Doe  
Then I should see the patient John Die in the list of patients  

## A curl example to search a patient by first name:
```bash
curl "http://127.0.0.1:5000/patients?first_name=John"

```


Scenario: Search a patient by surname  
Given I am a doctor Thomas  
And I have a patient John Doe with the social security number 123456789123456  
When I search for the patient with the surname Doe  
Then I should see the patient John Die in the list of patients 

## A curl example to search a patient by last_name name:
```bash
curl "http://127.0.0.1:5000/patients?last_name=Doe"

```

Scenario: Update a patient  
// done: the updation of patient is succesfully done with curl command and PUT request by using id number. I made it possible to update all files of 
// patient, including the first name, the last name, date of birth and SSN number. 
Given I am a doctor Thomas  
And I have a patient John Doe with the social security number 123456789123456  
When I update the patient John Doe with the social security number 123456789123457  
Then I should see the patient John Doe in the list of patients  
And his social security number should be 123456789123457  

## A curl example to update a patient:

```bash
curl -X PUT http://127.0.0.1:5000/patients/1 \
  -H "Content-Type: application/json" \
  -d '{"first_name": "John", "last_name": "Doe", "date_of_birth": "1980-01-01", "social_security_number": "123456789123457"}'
```


Scenario: Delete a patient  
// done: the deletion of patient is succesfully done with curl command and DELETE request by patient id number. 
Given I am a doctor Thomas  
And I have a patient John Doe with the social security number 123456789123456  
When I delete the patient John Doe  
Then I should not see the patient John Doe in the list of patients  

## A curl example to delete a patient:

```bash
curl -X DELETE http://127.0.0.1:5000/patients/1
```


Scenario: Get a patient by ID  
// done: the  of patient is succesfully done with curl command and GET REQUEST by using patient id number. 
Given I am a doctor Thomas  
And I have a patient John Doe with the ID 1  
When I get the patient with the ID 1  
Then I should see the patient John Doe informations

## A curl example to get a patient by id:

```bash
curl http://127.0.0.1:5000/patients/1
```

Getting a pagination for list of patients: 
We can define how many patients we can see for one page and separate patients into different pages


## A curl example to get a patient by id:

A ten patients for first page by default:
```bash
curl http://127.0.0.1:5000/patients/paginated
```

And pagination with parameters by each page: 
```bash
curl http://127.0.0.1:5000/patients/paginated?page=1&per_page=10
```
```bash
curl http://127.0.0.1:5000/patients/paginated?page=2&per_page=10
```

## Run tests using CLI

```bash
python -m unittest tests.patient.application.test_patient_service 
```

## Launch the application

```bash
 flask run
```
